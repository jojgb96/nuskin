<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('/data', function () {
    return view('data');
});
Route::get('/dashboard', function () {
    return view('dashboard');
});
Route::get('/pickupcustomer', function () {
    return view('dashboard');
});
Route::get('/login', function () {
    return view('login');
});
Route::get('/deliveryvendor', function () {
    return view('/dashboard');
});
Route::get('/notifdatevendor', function () {
    return view('/dashboard');
});
Route::get('/lostandfound', function () {
    return view('/dashboard');
});
Route::get('/lostandfoundapproval', function () {
    return view('/dashboard');
});
Route::get('/deliveryreport', function () {
    return view('/dashboard');
});
Route::get('/listcomplain', function () {
    return view('/dashboard');
});
Route::get('/listcomplain', function () {
    return view('/dashboard');
});
// Route::get('layout/sidebar', 'SidebarController@getSidebar');

Route::get('dashboard', 'SidebarController@getSidebar');

// Route::post('loginPost', 'loginController@loginPost')->name('login');

// Route::post('loginPost', 'loginController@loginPost')->name('login');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

///// upload csv files ////////
Route::get('/data', 'DeliveryVendorController@index');
// Route::get('/siswa/export_excel', 'SiswaController@export_excel');
Route::post('/deliveryvendor/import_excel', 'DeliveryVendorController@import_excel');
///// upload csv files ////////

