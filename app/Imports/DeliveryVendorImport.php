<?php

namespace App\Imports;

use App\DeliveryVendor;
use Maatwebsite\Excel\Concerns\ToModel;

class DeliveryVendorImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new DeliveryVendor([
            'date_order'=> $row[1],
            'so_number'=> $row[2],
            'item'=> $row[3],
            'no_aw'=> $row[4],
        ]);
    }
}
