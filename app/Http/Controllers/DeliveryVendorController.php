<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DeliveryVendor;
 
use Session;
 
use App\Exports\DeliveryReportExport;
use App\Imports\DeliveryVendorImport;
use Maatwebsite\Excel\Facades\Excel;


class DeliveryVendorController extends Controller
{
    public function index()
	{
        // $data = 'ada';
        // return $data;
        $deliveryvendor = deliveryvendor::all();
        
		return view('data',['deliveryvendor'=>$deliveryvendor]);
		// return view('data')->with('deliveryvendor', $deliveryvendor);
	}
 
	// public function export_excel()
	// {
	// 	return Excel::download(new SiswaExport, 'siswa.xlsx');
	// }
 
	public function import_excel(Request $request) 
	{
		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
 
		// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('DeliveryVen',$nama_file);
 
		// import data
		Excel::import(new DeliveryVendorImport, public_path('/DeliveryVen/'.$nama_file));
 
		// notifikasi dengan session
		Session::flash('sukses','Data Siswa Berhasil Diimport!');
 
		// alihkan halaman kembali
		return redirect('/data');
	}
    

}
